import React, { Component } from 'react';
import { useState } from 'react';
import './shop.scss';

const Shop = () => {
    // component state
    const [modalShown, setModalShown] = useState(false);
    const [modalPicture, setModalPicture] = useState('');
    const [planes, setPlanes] = useState([
        {
            aircraftModel: 'Cessna 172 Skyhawk',
            weightKg: '757',
            pictureUrl: 'https://flyawaysimulation.com/images/downloadshots/24390-fbvvjzip-14-image1.jpg'
        },
        {
            aircraftModel: 'Cessna 206 Stationair',
            weightKg: '1022',
            pictureUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Cessna.206h.stationair2.arp.jpg/800px-Cessna.206h.stationair2.arp.jpg'
        },
        {
            aircraftModel: 'Pilatus PC-12',
            weightKg: '2761',
            pictureUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Pilatus_PC-12-45%2C_Jetfly_Aviation_JP532772.jpg/800px-Pilatus_PC-12-45%2C_Jetfly_Aviation_JP532772.jpg'
        },
    ]);

    const toogle = (index) => {
        setModalPicture(planes[index].pictureUrl);
        setModalShown(!modalShown);
    }

    return (
        <div className='shop'>
            <header className='header'>
                <h1 className='h1'>Welcome to our shop</h1>
                <p>Please select from available planes</p>
            </header>
            <main className='main'>
                <table className='table-available-products'>
                    <thead>
                        <tr className='table-header'>
                            <td className='model'>Aircraft Model</td>
                            <td className='weight'>Weight [kg]</td>
                            <td className='picture'>picture</td>
                        </tr>
                    </thead>
                    <tbody className='table-body'>
                        {planes.map((plane, index) => {
                            return <tr key={Math.random() * 1000000}>
                                <td className='model'>{plane.aircraftModel}</td>
                                <td className='weight'>{plane.weightKg}</td>
                                <td className='picture'
                                    onClick={() => toogle(index)}
                                ><img src={plane.pictureUrl} /></td>
                            </tr>
                        })}
                    </tbody>
                </table>
            </main>
            <footer className='footer'>
                <div>virtual airlines</div>
            </footer>
            <div className={modalShown ? 'modal' : 'modal-hidden'}>
                <div className='container'>
                    <i className="close-icon fas fa-times" onClick={() => toogle(0)}></i>
                    <img className='picture'
                        src={modalPicture}>
                    </img>
                </div>
            </div>
        </div>);
}

export default Shop;